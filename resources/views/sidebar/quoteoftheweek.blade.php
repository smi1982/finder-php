<section class="sidebar quote-of-the-week col-sm-12 col-xs-6">
      <h3>Quote of the week</h3>
      <blockquote>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa velit, tempus id semper sed, tempus sit amet libero.
        <footer><cite>Johny Five</cite></footer>
      </blockquote>
      <a href="#" class="btn btn-primary" aria-label="See more quotes">More quotes</a>
</section>
