<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;

final class Article
{

  private static $json;
  private static $initialized = false;


  private function __construct() {
    //static class;
    throw new \Exception("static class, can't create");
  }

  private function __clone() {
    //static class;
    throw new \Exception("static class, can't clone");
  }

  private static function initialize()
{
  if (self::$initialized) {
    return;
  }
  self::$json = self::_loadJSON('data');
  self::$initialized = true;
}


  private static function _loadJSON($file) {
    $path = "json/${file}.json";
      if (!Storage::exists($path)) {
        throw new \Exception("Invalid File");
      }

      $file = Storage::get($path);
      $json = json_decode($file, true);
      return $json;
  }

  public static function getAll() {

    self::initialize();

    return self::$json;
  }

  public static function get($id) {

    self::initialize();

    if(!isset(self::$json[$id])) {
      throw new \Exception("Undefined offset ".$id);
    }
    return self::$json[$id];
  }

  public static function paginate($itemCount) {
    self::initialize();
    return new Paginator(self::$json, $itemCount);
  }


}
