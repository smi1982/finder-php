<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Finder.com demo - @yield('title')</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="css/main.min.css" media="all" rel="stylesheet" type="text/css" />
    </head>
    <body>
      <div class="container">
      <header class="col-xs-12">
        <h1>My awesome website</h1>
      </header>

      <nav class="navbar col-xs-12">
        <ul class="nav nav-pills" role="menu">
          <li class="active" role="menuitem"><a href="#">Home</a></li>
          <li role="menuitem"><a href="#">Members</a></li>
          <li role="menuitem"><a href="#">Photos</a></li>
          <li role="menuitem"><a href="#">Pages</a></li>
          <li role="menuitem"><a href="#">Discussions</a></li>
          <li role="menuitem"><a href="#">More</a></li>
        </ul>
      </nav>
      <main class="col-sm-8" aria-labelledby="main-label">
          @yield('content')
      </main>

      <aside class="col-sm-4">
        @include('sidebar.imageoftheweek')
        @include('sidebar.quoteoftheweek')
      </aside>
    </div>

    </body>
</html>
