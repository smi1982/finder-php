<?php

namespace App\Http\Controllers;

use App\Models\Article;

class ArticleController extends Controller
{
    public function index() {


      return view('main', ['articles' => Article::paginate(5)]); //TODO: possibly move elements counter to .env file 
    }

    public function api($id = null) {
      if($id === null) {
        return Article::getAll(); //lumen will create json from array by default;
      }
      return Article::get($id);
    }


    //
}
