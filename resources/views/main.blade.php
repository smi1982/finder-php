@extends('layouts.master')
@section('title', 'Index')


@section('content')
<h2 class="heading col-sm-12" id="main-label">Top five posts</h2>

@foreach($articles as $k => $v)
  <article class="row">
    <div class="col-sm-9 no-pad-right no-pad-left">
      <a href="{{$v['url']}}" aria-describedby="desc-{{$k}}">{{$v['title']}}</a>
      <p id="desc-{{$k}}" aria-hidden="true">{{$v['description']}}</p>
    </div>
    <div class="col-sm-3 hidden-xs no-pad-right no-pad-left">
      <img src="{{$v['thumbnail']}}" alt="{{$v['title']}}" />
    </div>
  </article>
@endforeach

@endsection
